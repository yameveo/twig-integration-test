<?php

namespace Infrastructure\Silex\Controller;

use Application\UseCase\FriendsUseCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FriendController
{
    /**
     * @var FriendsUseCase
     */
    protected $friendsUseCase;

    public function __construct(FriendsUseCase $friendsUseCase)
    {
        $this->friendsUseCase = $friendsUseCase;
    }

    public function indexAction(Request $request)
    {
        $friends = $this->friendsUseCase->execute(
            $request->get('limit'),
            $request->get('offset')
        );
        return new JsonResponse($friends);
    }
}